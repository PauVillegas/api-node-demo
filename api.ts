import dotenv from 'dotenv';
import express, {Request, Response} from 'express';
import { MongoClient } from 'mongodb';
import { request } from 'http';
import { json } from 'stream/consumers';
import cors from 'cors';

// Variable para la cadena de conexión a MongoDB 
let url: string = ``;

dotenv.config({debug: false});

// Evaluación de variables de entorno según ambiente
if (process.env.NODE_ENV == 'DEV') {
    // Ambiente DEV | Sin credenciales (sin usuario y contraseña)
    url = `mongodb://${process.env.DELASALLE_MONGO_HOST}:${process.env.DELASALLE_MONGO_PORT}/${process.env.DELASALLE_MONGO_DATABASE}` //mongodb://HOST:PORT/DB
} else if (process.env.NODE_ENV == 'QA') {
    // Ambiente QAS | Con autenticación
    url = `mongodb://${process.env.DELASALLE_MONGO_USER}:${process.env.DELASALLE_MONGO_PASSWORD}@${process.env.DELASALLE_MONGO_HOST}:${process.env.DELASALLE_MONGO_PORT}/${process.env.DELASALLE_MONGO_DATABASE}`
} else {
    // Ambiente PRO | Con autenticación y en modo cluster
    url = `mongodb://${process.env.DELASALLE_MONGO_USER}:${process.env.DELASALLE_MONGO_PASSWORD}@${process.env.DELASALLE_MONGO_HOST_NODE01}:${process.env.DELASALLE_MONGO_PORT01},${process.env.DELASALLE_MONGO_HOST_NODE02}:${process.env.DELASALLE_MONGO_PORT02},${process.env.DELASALLE_MONGO_HOST_NODE03}:${process.env.DELASALLE_MONGO_PORT03}/${process.env.DELASALLE_MONGO_DATABASE}?rs=${process.env.DELASALLE_MONGO_RS}`
}

console.log(`La cadena de conexión es ${url}`);

//console.log(`La variable de entorno para el servidor es ${process.env.DELASALLE_MONGO_HOST}`);
/*
const nombre: string = `Paulina Villegas`;
const edad: number = 34;
let domicilio = "Jardines de Versalles";

console.log(`Hola NodeJS... Te saluda ${nombre}`);
console.log(`Domicilio 1 ${domicilio}`);
*/

const Clientdb = new MongoClient(url);

const api = express();

api.use(cors());

api.get('/health', (req: Request, res: Response) => {
    res.status(200).json({
        status: 'OK',
        data: 'Success'
    })
})

api.get('/about', (req: Request, res: Response) => {
    getDataAbout()
        .then((about: any) => {
            res.status(200).json({
                status: 'OK',
                title: 'Acerca de...',
                logo: '',
                data: about
            });
        })
        .catch((err: any) => {
            res.status(500).json({
                status: 'FAIL',
                title: 'Error from about...',
                logo: '',
                data: err
            });
        });
})

api.get('/dc', (req: Request, res: Response) => {
    getDataDC()
        .then((dc: any) => {
            res.status(200).json({
                status: 'OK',
                title: 'Hellow from DC Comics Heroes...',
                logo: '',
                data: dc
            });
        })
        .catch((err: any) => {
            res.status(500).json({
                status: 'FAIL',
                title: 'Error from DC Comics Heroes...',
                logo: '',
                data: err
            });
        })
        .finally(() => Clientdb.close());

    //const dc: any = await getDataDC()
    //Demo
    //res.status(200).json({
    //    status: 'OK',
    //    title: 'Hellow from DC Comics Heroes...',
    //    logo: '',
    //    data: []
    //})
})

api.get('/marvel', (req: Request, res: Response) => {
    getDataMarvel()
        .then((marvel: any) => {
            res.status(200).json({
                status: 'OK',
                title: 'Hellow from Marvel Comics Heroes...',
                logo: '',
                data: marvel
            });
        })
        .catch((err: any) => {
            res.status(500).json({
                status: 'FAIL',
                title: 'Error from Marvel Comics Heroes...',
                logo: '',
                data: err
            });
        })
        .finally(() => Clientdb.close());

    //res.status(200).json({
    //    status: 'OK',
    //    title: 'Hellow from Marvel Comics Heroes...',
    //    logo: '',
    //    data: []
    //})
})

api.listen(3000, () => {
    console.log(`Servidor Express funcionando correctamente en puerto 3000`);
});


const getDataAbout:any = async () => {
    return await "{\"Nombre\":\"Rosa Paulina Villegas Domínguez\",\"Proyecto\":\"Proyecto Final en Angular con Tecnologías Web de Código Abierto\",\"Maestria\":\"Maestría en Tecnologías Web y Dispositivos Móviles\"}";
}

const getDataDC:any = async () => {
    //Conexión a MongoDB
    await Clientdb.connect();
    //Selección de base de datos
    const db = Clientdb.db(process.env.DELASALLE_MONGO_DATABASE);

    return await db.collection('heroes').find({publisher: "DC Comics"}).toArray();
}

const getDataMarvel:any = async () => {
    await Clientdb.connect();
    const db = Clientdb.db(process.env.DELASALLE_MONGO_DATABASE);

    return await db.collection('heroes').find({publisher: "Marvel Comics"}).toArray();
}
